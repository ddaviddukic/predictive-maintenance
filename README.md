# USE IOT TO ENABLE PREDICTIVE MAINTENANCE 
#### Peaky Miners  (David Dukić, Josip Domazet, Ivan Landeka) 
#### Final Result: 1st place (39 teams total)
Project contains source code and report for LUMEN Data Science challenge, Zagreb, 2020 <br/>
https://www.estudent.hr/projekti/lumen-data-science

## LUMEN_Data_Science_Task_2020

* LUMEN Data Science task for year 2020

## Peaky_Miners_Documentation

* Project documentation of our solution
* Structure following CRISP-DM metodology

## Peaky_Miners_Technical_Documentation

* Description of used programming languages and libraries
* Also contains project directory structure 

## Peaky_Miners_Final_Presentation

* LaTeX Beamer presentation 
* Used to present solution during the final (6.6.2020)
